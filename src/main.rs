mod strat;
mod nrolls;
mod targetscore;

use strat::Strat;
use nrolls::NRolls;
use targetscore::TargetScore;

use std::{env, error::Error, fs::File, io::prelude::*};


fn main() -> Result<(), Box<dyn Error>> {
    // skip first arg
    let mut args = env::args().skip(1);
    let k: u64 = args.next().expect("Usage: main.exe <K>").parse()?;

    // add and test strats
    let mut ranking: Vec<(String, f64, f64)> = Vec::new();
    for n in 1..10 {
        let strat = NRolls::new(n);

        let name = format!("{}", strat);
        let (rounds, points) = strat.play_many(k);

        println!("{}", name);
        ranking.push((name, rounds, points));
    }

    for t in 10..40 {
        let strat = TargetScore::new(t);

        let name = format!("{}", strat);
        let (rounds, points) = strat.play_many(k);

        println!("{}", name);
        ranking.push((name, rounds, points));
    }

    // write results
    let mut file = File::create("results.csv")?;
    file.write_all(b"name,rounds,points\n")?;
    for (name, rounds, points) in ranking.iter() {
        file.write_all(format!("{},{},{}\n", name, rounds, points).as_bytes())?;
    }

    // print results
    ranking.sort_unstable_by(|(_na, _ra, pa), (_nb, _rb, pb)| pb.partial_cmp(pa).unwrap());
    for (i, (name, rounds, points)) in ranking.iter().enumerate() {
        println!("{:2}. [{:8.6} points in {:8.6} rounds] {}", i + 1, points, rounds, name);
    }

    Ok(())
}
