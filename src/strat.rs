use rand::{self, distributions::{Distribution, Uniform}};
use rayon::prelude::*;
use std::fmt;


struct Avg {
    avg: f64,
    n: u64,
}

impl Avg {
    fn new() -> Self {
        Avg { avg: 0.0, n: 0 }
    }

    fn add(mut self, x: f64) -> Self {
        self.n += 1;
        self.avg += (x - self.avg) / (self.n as f64);

        self
    }

    fn merge(mut self, other: Self) -> Self {
        let n = self.n + other.n;
        self.avg = self.avg * (self.n as f64 / n as f64) + other.avg * (other.n as f64 / n as f64);
        self.n = n;

        self
    }

    fn value(self) -> f64 {
        self.avg
    }
}


pub trait Strat: fmt::Display + Sync {
    fn should_roll_again(&self, n: u64, points: u64) -> bool;

    fn play_once(&self, die_values: &Uniform<u64>) -> (u64, u64) {
        let mut rng = rand::thread_rng();

        let mut n = 0;
        let mut points = 0;
        while self.should_roll_again(n, points) {
            let value = die_values.sample(&mut rng);
            n += 1;

            if value == 1 {
                points = 0;
                break;
            }
            else {
                points += value;
            }
        }

        (n, points)
    }

    fn play_many(&self, k: u64) -> (f64, f64) {
        let die_values = Uniform::new_inclusive(1, 6);

        let (n, points) = (0..k).into_par_iter()
            .map(|_| self.play_once(&die_values))
            .fold(|| (Avg::new(), Avg::new()), |(n_avg, p_avg), (n, p)| (n_avg.add(n as f64), p_avg.add(p as f64)))
            .reduce(|| (Avg::new(), Avg::new()), |(n, p), (m, q)| (n.merge(m), p.merge(q)));

        (n.value(), points.value())
    }
}
