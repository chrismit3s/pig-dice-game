use crate::strat::Strat;
use std::fmt;


pub struct TargetScore {
    target: u64,
}

impl TargetScore {
    pub fn new(target: u64) -> Self {
        TargetScore { target }
    }
}

impl Strat for TargetScore {
    fn should_roll_again(&self, _n: u64, points: u64) -> bool {
        points < self.target
    }
}


impl fmt::Display for TargetScore {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "target{:02}", self.target)
    }
}
