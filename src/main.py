from random import random


class Strat():
    def should_roll(self, n, score):
        raise NotImplementedError


class RollN(Strat):
    def __init__(self, n):
        self.max_n = n

    def __str__(self):
        return f"[{self.max_n} rolls]"

    def should_roll(self, n, score):
        return n < self.max_n


class ScoreStop(Strat):
    def __init__(self, target):
        self.target = target

    def __str__(self):
        return f"[stop at {self.target:2}]"

    def should_roll(self, n, score):
        return score < self.target


def roll_die():
    return int(random() * 6) + 1


def test_strat(strat, k=1000000):
    n_total = 0
    score_total = 0
    for _ in range(k):
        n, score = test_strat_once(strat)
        n_total += n
        score_total += score

    n_total /= k
    score_total /= k
    return (n_total, score_total)


def test_strat_once(strat):
    score = 0
    n = 0
    while strat.should_roll(n, score):
        value = roll_die()
        n += 1

        if value == 1:
            score = 0
            break
        else:
            score += value
    return (n, score)


if __name__ == "__main__":
    ranking = []

    for n in range(1, 10):
        strat = RollN(n)
        ranking.append((strat, *test_strat(strat)))
        print(".", end="", flush=True)

    for t in [2, 4, 6, 8, 10, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 28, 30]:
        strat = ScoreStop(t)
        ranking.append((strat, *test_strat(strat)))
        print(".", end="", flush=True)

    ranking.sort(key=lambda x: -x[2])
    print("\n".join(f"{str(strat) + ':':20} {n:4.2f} rolls, {score:5.2f}" for strat, n, score in ranking))
