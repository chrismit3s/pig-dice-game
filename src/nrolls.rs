use crate::strat::Strat;
use std::fmt;


pub struct NRolls {
    n: u64,
}

impl NRolls {
    pub fn new(n: u64) -> Self {
        NRolls { n }
    }
}

impl Strat for NRolls {
    fn should_roll_again(&self, n: u64, _points: u64) -> bool {
        n < self.n
    }
}


impl fmt::Display for NRolls {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}rolls", self.n)
    }
}
